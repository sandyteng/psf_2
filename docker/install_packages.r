#install R packages
install.packages("ggplot2", repos='http://cran.us.r-project.org')
install.packages("dplyr", repos='http://cran.us.r-project.org')
install.packages("remotes", repos='http://cran.us.r-project.org')
library(remotes)
remotes::install_version("cowplot", "0.9.2")