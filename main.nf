//enable DSL2
nextflow.enable.dsl = 2
include {blastdb_generation; blat; megablast; write_infofiles as write_blat; write_infofiles as write_megablast; extract_IDs as extract_blat_IDs; extract_IDs as extract_megablast_IDs; merge_sort_intersection; merge_sort_union; extract_pseudo_candidates; generate_candidate_fasta; blastn; summarize_results; create_pseudolist} from './modules/modules'

//import variables from .json file (-params-file)
refgenome = Channel.of(params.refgenome)
dbasestr = Channel.from(params.dbasestr)
amplicon = Channel.of(params.amplicon)
pycode_list = Channel.of(params.pycode_list)
pycode_info = Channel.of(params.pycode_info)
pycode_union = Channel.of(params.pycode_union)
pycode_all = Channel.of(params.pycode_all)
pycode_summary = Channel.of(params.pycode_summary)
pycode_pseudolist = Channel.of(params.pycode_pseudolist)

/*my workflow*/
workflow Pseudo_finder {
    take:
    //sequences 
    refgenome
    amplicon
    //the name of ref. sequence
    dbasestr
    //python codes
    pycode_info
    pycode_list
    pycode_all
    pycode_union
    pycode_summary
    pycode_pseudolist

    main:
    blastdb_generation(refgenome,dbasestr)
    blat(amplicon,refgenome)
    megablast(dbasestr,amplicon,blastdb_generation.out.blastdb)
    write_blat("blat",blat.out.result_blat,pycode_info)
    write_megablast("megablast",megablast.out.result_megablast,pycode_info)
    extract_blat_IDs("blat",write_blat.out.infofile,pycode_list,pycode_all)
    extract_megablast_IDs("megablast",write_megablast.out.infofile,pycode_list,pycode_all)
    merge_sort_intersection(extract_blat_IDs.out.candidatelist,extract_megablast_IDs.out.candidatelist)
    merge_sort_union(extract_blat_IDs.out.fileall,extract_megablast_IDs.out.fileall)
    extract_pseudo_candidates(merge_sort_intersection.out.fileSingle,merge_sort_union.out.fileAll,pycode_union)
    generate_candidate_fasta(amplicon,extract_pseudo_candidates.out.pseudolist)
    /*debug lines*/
    blastn(dbasestr,generate_candidate_fasta.out.subfasta,blastdb_generation.out.blastdb)
    summarize_results(pycode_summary,blastn.out.result_blastn)
    create_pseudolist(pycode_pseudolist,summarize_results.out.blastn_keyinfo)
    
    emit:
    create_pseudolist.out.report
}
workflow {
    Pseudo_finder(refgenome,amplicon,dbasestr,pycode_info,pycode_list,pycode_all,pycode_union,pycode_summary,pycode_pseudolist)
}