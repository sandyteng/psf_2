### The development of Pseudogene detection pipeline 

Author: Sandy Teng <sandyteng@actgenomics.com>

#### Code to build the docker image
* build a docker image
    - cd ./docker/
    - docker build -t sandyteng/pseudogene_sandy:v1.0.0 --no-cache .

* run a docker container
    - docker run -it sandyteng/pseudogene_sandy:v1.0.0 /bin/bash

#### Code for file conversion
* convert bed file to fasta file
    - bedtools getfasta [OPTIONS] -fi <input FASTA> -bed <BED/GFF/VCF> -nameOnly > <inputname>.read.fasta

#### Sample code for (hg19 & GRCh38) containerized pipeline execution

<p>**local workflows**<br> 

<p>(local dockerized hg19 pipeline)<br> 
nextflow run main.nf \<br>-params-file ./params/Onco2M7_hg19.json \<br>-c pseudogene_localdocker.config</p>

<p>(local dockerized hg19 pipeline)<br>
nextflow run main.nf \<br>-params-file ./params/PA031_hg19.json \<br>-c pseudogene_localdocker.huge.config</p>
<p>Remark: 32.G memory usage (per process) is specified in "pseudogene_localdocker.huge.config"</p>

<p>(local dockerized GRCh38 pipeline)<br>
nextflow run main_grch38.nf \<br>-params-file ./params/PA039_GRCh38.json \<br>-c pseudogene_localdocker.config</p>

#### Pipeline description 
##### Remark: The pipeline results are valid only if all the input amplicon names are unique. Please check if there is any duplicated name in the input amplicon file.

* Parameters: (./params/)
    - amplicon: amplicon fasta file
    - refgenome: reference genome/database fasta file (e.g. /mnt/BI3/Team_workdir/sandyteng_workdir/PseudoGene_pipeline/refgenome/GRCh38.p2.mask1.fasta, /BI3/Team_workdir/sandyteng_workdir/PseudoGene_pipeline/refgenome/hg19.fasta)
    - pycode_list: the directory for “extract_single_hits_from_infofiles.py”
    - pycode_info: the directory for “blat_info_arg.py”
    - pycode_union: the directory for “extract_ids_from_pseudolists.py”
    - pycode_all: the directory for “extract_all_hits_from_infofiles.py”
    - pycode_summary: the directory for “blat_keyinfo_positions_arg.py”
    - pycode_pseudolist: the directory for “blastn_stage2_pseudogenelist_arg.py”
    - dbasestr: reference genome/database string (example 1: database file: hg19.fasta -> corresponding database string: “hg19”; example 2: database file: GRCh38.p2.mask1.fasta -> corresponding database string: “GRCh38.p2.mask1”
    - publish_dir: result directory

* Modules: (./modules/)
    -  blastdb_generation: generate blast database for specified reference genome ("makeblastdb")
      - hg19 (./modules/modules.nf)
      - GRCh38 (./modules/GRCh38_db.nf)
    -  megablast: generate megablast alignments ("blastn")
    -  blat: generate blat alignments ("blat")
    -  write_blat: generate blat.info.csv ("blat_info_arg.py")
    -  extract_blat_IDs: extract amplicon ids with single hit (from blat alignments) ("extract_single_hits_from_infofiles.py","extract_all_hits_from_infofiles.py")
    -  write_megablast: generate megablast.info.csv ("blat_info_arg.py")
    -  extract_megablast_IDs: extract amplicon ids with single hit (from megablast alignments) ("extract_single_hits_from_infofiles.py","extract_all_hits_from_infofiles.py")
    -  merge_sort_union: extract the amplicon ids identified by blat or megablast as single hit ("cat","sort","uniq")
    -  merge_sort_intersection: extract the consistent amplicon ids with single hit ("cat","sort","uniq -d")
    -  extract_pseudo_candidates: generate pseudogene candidates (# amplicon IDs - # IDs with consistent single hit) ("extract_ids_from_pseudolists.py")
    -  generate_candidate_fasta: obtain fasta for pseudogene candidates ("seqtk subseq")
    -  blastn: generate blastn results for the candidate subset ("blastn -task blastn")
    -  summarize_results: summarize blastn results for valid alignments ("blat_keyinfo_positions_arg.py")
    -  create_pseudolist: generate a list of identified pseudogene amplicons ("blastn_stage2_pseudogenelist_arg.py")

* Output files:
    - blastn_stage2.ps.key.info.csv (blastn results of valid alignments (aligned length / amplicon length == 1, identity >= 90%, gaps + gap opening <= 5))
    - pseudolist.report (a list of identified pseudogene amplicons)
  
* Utilities: (./utilities/)
    - blastnsummarytable.py: summarize blastn results for pseudogene amplicons to a .csv file (e.g. blastntable_XXX.csv) and generate a .txt file (e.g. countnum_XXX.txt) comprising total amplicon counts, single hit amplicon counts and pseudogene amplicon counts (format: # of amplicons, # of single hits, # of pseudogenes)