#!/usr/bin/python3
#======
# [Development version]
# 20221117: parse amplicon.read.fasta, pseudolist.report and blastn_stage2.ps.key.info.csv files to obtain single/pseudo transcript counts and an output (blastn result) summary table 
#
# Program: blastnsummarytable.py  
# Usage: python3 blastnsummarytable.py -f /mnt/BI3/Team_workdir/sandyteng_workdir/PseudoGene_pipeline/data/PA015_ACTMonitorLungv2_amplicon_hg19.new.read.fasta
#                                      -t /mnt/BI3/Team_workdir/sandyteng_workdir/PseudoGene_pipeline_results/PA015/pseudolist.report
#                                      -kinfo /mnt/BI3/Team_workdir/sandyteng_workdir/PseudoGene_pipeline_results/PA015/blastn_stage2.ps.key.info.csv
#                                      -cout /mnt/BI3/Team_workdir/sandyteng_workdir/PseudoGene_pipeline_results/PA015/countnum_PA015.txt
#                                      -stable /mnt/BI3/Team_workdir/sandyteng_workdir/PseudoGene_pipeline_results/PA015/blastntable_PA015.csv           
# Description: a program to obtain pseudo transcript count & single hit count & an blastn alignment table of filtered alignments for identified pseudo transcripts 
# Author: sandyteng (sandyteng@actgenomics.com)
#
#======
class pkg():
    def __init__(self, args = {}):
        self.args = {}
        self.args.update(args)
        self.vars = {}
        self.prepare()

    def prepare(self):
        # pass information from args to vars
        self.vars['fastadir'] = self.args['fastadir']
        self.vars['targetfile'] = self.args['targetfile']
        self.vars['keyinfofile'] = self.args['keyinfofile']
        self.vars['countnumfile'] = self.args['countnumfile']
        self.vars['summarytablefile'] = self.args['summarytablefile']
    #
    # Function description: your function description
    # Goal: the goal of this function
    # 
    # a function to load read.fasta (amplicon.read.fasta) file
    def loadreadfasta(self,fastafiledir):
        """
        filedir = "/mnt/BI3/Team_workdir/sandyteng_workdir/PseudoGene_pipeline/data/PA015_ACTMonitorLungv2_amplicon_hg19.new.read.fasta"
        fastadict = loadreadfasta(filedir)
        """
        ifh = open(fastafiledir)
        file_content = ifh.read()
        txtLst = file_content.split('>')
        # close file
        ifh.close()
        # parse fasta file
        resdict = dict() # a dict. to record amplicon sequence
        countdict = dict() # a dict. to record unique amplicon id
        duplicateflag = 0
        for line in txtLst[1:]:
            infolist = line.split("\n")
            if infolist[0] not in resdict:
                resdict[infolist[0]] = infolist[1] # key: read id, value: sequence
                countdict[infolist[0]] = 1
            else: # code to check if any duplicates exists in the amplicon fasta file
                countdict[infolist[0]] += 1
                newname = infolist[0] + "_" + str(countdict[infolist[0]])
                resdict[newname] = infolist[1]
                print(newname)
                duplicateflag = 1
        if duplicateflag == 1:
            return(False)
        else:
            return(resdict)  
    # a function to generate a table of potential alignments for pseudo transcripts 
    def getblastnsummary(self,targetfiledir,keyinfofiledir):
        """
        targetfile = "/mnt/BI3/Team_workdir/sandyteng_workdir/PseudoGene_pipeline_results/PA015/pseudolist.report"
        keyinfofile = "/mnt/BI3/Team_workdir/sandyteng_workdir/PseudoGene_pipeline_results/PA015/blastn_stage2.ps.key.info.csv"
        resdf = getblastnsummary(targetfile,keyinfofile)
        """
        # read files
        tf = open(targetfiledir)
        tfcontent = tf.read()
        targetlist = tfcontent.strip("\n").split("\n")
        df = pd.read_csv(keyinfofiledir)
        # generate output table
        singledf = df[df["Query_ID"].isin(targetlist)]
        return(singledf)
    def main(self):
        # main code
        try:
            fastadict = self.loadreadfasta(self.vars['fastadir'])
            resdf = self.getblastnsummary(self.vars['targetfile'],self.vars['keyinfofile'])
            if fastadict==False:
                print("your fasta file contains duplicated amplicon id")
                raise
        except:
            print("Your fasta file can not be successfully opened.")
        # generate a summary table for pseudo transcripts 
        resdf.to_csv(self.vars['summarytablefile'],index=False)
        # record count number
        ofh = open(self.vars['countnumfile'],"w")
        seqnum = len(fastadict) # total 
        pseudonum = len(resdf["Query_ID"].unique()) # pseudo transcripts
        singlenum = seqnum - pseudonum # single hits
        numlist = [seqnum,singlenum,pseudonum]
        print(self.vars['fastadir'],file=ofh)
        print(",".join(map(str,numlist)),file=ofh)
        # close file
        ofh.close()
if __name__ == '__main__':
# code comment is required
    import argparse
    import pandas as pd
    parser = argparse.ArgumentParser(description='A program to summarize the results of pseudogene detection pipeline.')
    parser.add_argument("-f", "--fastadir", help="file directory for read.fasta file", required=True, type=str)
    parser.add_argument("-t", "--targetfile", help="file directory for pseudo list", required=True, type=str)
    parser.add_argument("-kinfo", "--keyinfofile", help="file directory for keyinfo.csv", required=True, type=str)
    parser.add_argument("-cout", "--countnumfile", help="file directory for transcript count table", required=True, type=str)
    parser.add_argument("-stable", "--summarytablefile", help="file directory of the blastn alignment details of the identified pseudo transcripts", required=True, type=str)
    args = parser.parse_args()
    a = pkg(vars(args))
    a.main()