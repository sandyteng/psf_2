//**All modules 
/*A process to generate blast (GRCh38) database*/
process blastdb_generation {
    publishDir "${params.publish_dir}/", mode: 'copy', enabled: params.publish_dir as boolean
    input:
    path refseq
    output:
    path('GRCh38.p2.mask1*', emit: blastdb) 
    script:
    """
    makeblastdb -dbtype nucl \
    -in $refseq \
    -title GRCh38.p2.mask1 \
    -out GRCh38.p2.mask1
    """
}