//**All modules 
/*A process to generate blast database*/
process blastdb_generation {
    publishDir "${params.publish_dir}/", mode: 'copy', enabled: params.publish_dir as boolean
    input:
    path refseq
    val dbasestr 
    output:
    path('hg19*', emit: blastdb) 
    script:
    """
    makeblastdb -dbtype nucl \
    -in $refseq \
    -title $dbasestr \
    -out $dbasestr
    """
}
process blat {
    publishDir "${params.publish_dir}/", mode: 'copy', enabled: params.publish_dir as boolean
    input:
    path targetseq
    path refseq 
    output:
    path('blatoutput_blast.psl', emit: result_blat)
    script:
    """
    blat -out=blast8 $refseq $targetseq blatoutput_blast.psl
    """
}
process megablast {
    publishDir "${params.publish_dir}/", mode: 'copy', enabled: params.publish_dir as boolean
    input:
    val dbasestr
    path targetseq
    path dbasefile
    output:
    path('megablast_tabular.txt', emit: result_megablast)
    script:
    """
    blastn \
    -query $targetseq \
    -db $dbasestr \
    -dust no -soft_masking false \
    -outfmt "6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore gaps positive" \
    -out megablast_tabular.txt   
    """
}
process write_infofiles {/*generate blat.info.csv & megablast.info.csv*/
    publishDir "${params.publish_dir}/", mode: 'copy', enabled: params.publish_dir as boolean
    input:
    val TASK
    path aligned_result
    path CODE1
    output:
    path('*info.csv', emit: infofile)  
    script:
    """
    python3 $CODE1 -i $aligned_result -o $TASK
    """
}
process extract_IDs { 
    /*process to generate .single & .all*/
    /*A .single file is a list to store amplicon IDs considered to be single hits using aligner blat/megablast*/
    /*A .all file is a list to store all amplicon IDs in (blat/megablast).info.csv */
    publishDir "${params.publish_dir}/", mode: 'copy', enabled: params.publish_dir as boolean
    input:
    val TASK 
    path infofile
    path CODE2
    path CODE3
    output:
    path('*.single', emit: candidatelist) 
    path('*.all', emit: fileall)
    script:
    """
    python3 $CODE2 -i $infofile -o $TASK
    python3 $CODE3 -i $infofile -o $TASK
    """
}
process merge_sort_intersection {
    publishDir "${params.publish_dir}/", mode: 'copy', enabled: params.publish_dir as boolean
    input:
    path file1 
    path file2 
    output:
    path('fileSingle', emit: fileSingle)
    script:
    """
    cat $file1 $file2 | sort | uniq -d > fileSingle 
    """
}
process merge_sort_union {
    publishDir "${params.publish_dir}/", mode: 'copy', enabled: params.publish_dir as boolean
    input:
    path file1 
    path file2
    output:
    path('fileAll', emit: fileAll) 
    script:
    """
    cat $file1 $file2 | sort | uniq > fileAll 
    """
}
process extract_pseudo_candidates {/*process to generate pseudogene candidates (#amplicon IDs - #consistent single hits)*/
    publishDir "${params.publish_dir}/", mode: 'copy', enabled: params.publish_dir as boolean
    input:
    path singleFile
    path allFile 
    path CODE4
    output:
    path('*subset', emit: pseudolist)
    script:
    """
    python3 $CODE4 -i $allFile -ii $singleFile -o pseudocandidates -t d
    """
}
process generate_candidate_fasta {/*process to generate an amplicon subset consisting of pseudogene candidates*/
    publishDir "${params.publish_dir}/", mode: 'copy', enabled: params.publish_dir as boolean
    input:
    path targetseq
    path pseudolist
    output:
    path('selected_fasta', emit: subfasta) 
    script:
    """
    seqtk subseq $targetseq $pseudolist > selected_fasta
    """
}
process blastn {/*generate blastn result for the amplicon subset*/
    publishDir "${params.publish_dir}/", mode: 'copy', enabled: params.publish_dir as boolean
    input:
    val dbasestr
    path selected_fasta
    path dbasefile
    output:
    path('blastn_result.txt', emit: result_blastn)
    script:
    """
    blastn \
    -query $selected_fasta \
    -task blastn \
    -db $dbasestr \
    -dust no -soft_masking false \
    -outfmt "6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore gaps positive" \
    -out blastn_result.txt \
    -max_target_seqs 5 \
    -num_threads 8
    """
}
/*summarize output*/
process summarize_results {
    /*publishDir "/mnt/BI3/Team_workdir/sandyteng_workdir/nextflow/RESULT/"*/ //specify directory here to store the output
    publishDir "${params.publish_dir}/", mode: 'copy', enabled: params.publish_dir as boolean
    input:
    path CODE5
    path result_blastn
    output:
    path('blastn_stage2*', emit: blastn_keyinfo)
    script:
    """
    python3 $CODE5 -i $result_blastn -o blastn_stage2
    """
}
process create_pseudolist {
    /*publishDir "/mnt/BI3/Team_workdir/sandyteng_workdir/test/RESULT/", mode: 'copy' */
    publishDir "${params.publish_dir}/", mode: 'copy', enabled: params.publish_dir as boolean
    input:
    path CODE6
    path stage2_infocsv 
    output:
    path('pseudolist*', emit: report)
    script:
    """
    python3 $CODE6 -i $stage2_infocsv -o pseudolist
    """
}