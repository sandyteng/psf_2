#!/usr/bin/python3
#======
#
# Program: A Program to genearate a pseudogenelist (a filter for the stage2-blastn result (<blastn result>.ps.key.info.csv))
# Usage: obtain amplicons that have at least one secondary alignment (Remark: primary alignment == amplicon itself) statisfied the following criteria:
# 1. completely aligned to its corresponding ref. sequence (i.e., aligned ratio == 1)
# 2. having identity >= 90
# 3. having gap sum (Remark: gap sum = gap openings + gaps) <= 5 
# Description: 
# Inputs: -i <blastn result>.ps.key.info.csv -o <pseudogenelist>.report
# Author: sandyteng (sandyteng@actgenomics.com)
#
#======
import pandas as pd

class pkg():
    def __init__(self, args = {}):
        self.args = {}
        self.args.update(args)
        self.vars = {}
        self.prepare()

    def prepare(self):
        # pass information from args to vars
        self.vars['input_file'] = self.args['input_file'] 
        self.vars['output_file'] = self.args['output_file']
        
    def main(self):
		# your code
        try:
            sumfile = pd.read_csv(self.vars['input_file']) #sumfile = pd.read_csv("/mnt/BI3/Team_workdir/sandyteng_workdir/python_code/Infofiles/blastn_tabular_stage2_477_5.ps.key.info.csv")
        except:
            print("your file does not exist!")
        # main code
        outname = args.output_file + ".report"
        outfile = open(outname,'w')
        
        #add "Gap sum" column 
        sumfile["Gap sum"] = sumfile["gap openings"] + sumfile["gaps"]
        Result = sumfile
        #create "Aligned ratio" column
        Result["Aligned ratio"] = Result["Mlen"]/Result["Self"]
        #function for pseudogene extraction 
        def Pseudo_seek2(df):
            all = df.drop_duplicates(subset=['Query_ID'])
            single = df.drop_duplicates(subset=['Query_ID'],keep=False)
            allset = set(list(all['Query_ID']))
            singleset = set(list(single['Query_ID']))
            multiset = allset.difference(singleset)
            return(multiset)
        
        #subsetB = Result[(Result["Aligned ratio"]==1)] #completely aligned ==> 2433 #completely aligned         
        subsetC = Result[(Result["Aligned ratio"]==1) & (Result["Gap sum"]<=5)] #completely aligned && gap sum <= 5       
        #count # of unfiltered pseudogenes
        setC = Pseudo_seek2(subsetC)
        # check id 
        setClist = list(setC)
        Count = 0
        Checked_pseudo = list()#112
        Discarded_pseudo = list()#21
        testlist = list(subsetC["Query_ID"])
        for i in range(len(setC)):
            Num = testlist.count(setClist[i])
            if Num >= 2:
                Count+=1
                idlist = list(subsetC[subsetC["Query_ID"]==setClist[i]]['id'])
                idlist.sort()
                if idlist[len(idlist)-2] >= 90:
                    Checked_pseudo.append(setClist[i])
                else:
                    Discarded_pseudo.append(setClist[i])
        #Create a pseudolist 
        for ampID in Checked_pseudo:
            print(ampID,file=outfile)
        # close files
        outfile.close()

if __name__ == '__main__':
# code comment is required
    import argparse
    parser = argparse.ArgumentParser(description='Read info from your input file and create an output file.')
    parser.add_argument("-i", "--input_file", help="input file.", required=True, type=str)
    parser.add_argument("-o", "--output_file", help="result table.", required=True, type=str)
    args = parser.parse_args()
    a = pkg(vars(args))
    a.main()