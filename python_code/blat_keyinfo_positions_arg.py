#!/usr/bin/python3
#======
#
# Program: python code for blat/blast result (key columns) extraction (record only valid alignment)
# Usage: 
# enter a blat/blast result (wt 12/14 columns) & an output directory 
# Description: create a .ps.key.info.csv file containing following info:
# Hclipped: q.start -1
# Mlen: q.end - q.start +1
# Tclipped: q.end(self) - q.end
# Distance, Self
# mismatches, gap openings, id, (gaps)
# Author: sandyteng (sandyteng@actgenomics.com)
#
#======
import pandas as pd

class pkg():
    def __init__(self, args = {}):
        self.args = {}
        self.args.update(args)
        self.vars = {}
        self.prepare()

    def prepare(self):
        # pass information from args to vars
        self.vars['input_file'] = self.args['input_file'] #blast result, e.g., /mnt/BI3/Team_workdir/sandyteng_workdir/blast/blastn_tabular_blastn_stage2.txt
        self.vars['output_dir'] = self.args['output_dir'] #output directory, e.g., /mnt/BI3/Team_workdir/sandyteng_workdir/python_code/RESULTS/blastn_stage2
    ## a function to obtain selected alignment rows
    def Record_blat_raw2(self,blastfile,selectedlist):
        f = open(blastfile)
        txtLst = f.readlines()
        f.close()
        Num_col = len(txtLst[0].strip().split("\t"))
        if Num_col==12:
            varname = ['Query','Subject','id','alignment length','mismatches','gap openings','q.start','q.end','s.start','s.end','e-value','bit score']
        elif Num_col==14:
            varname = ['Query','Subject','id','alignment length','mismatches','gap openings','q.start','q.end','s.start','s.end','e-value','bit score','gaps','positive']
        else:
            return("Please enter a blat/blast result with 12 or 14 columns.")          
        # create a dict to store alignment info    
        ibdict = dict([])
        for rowidx in selectedlist:
            txtLine = txtLst[rowidx].strip().split()
            ibdict[rowidx] = {}
            for j in range(len(varname)):
                ibdict[rowidx][varname[j]] = txtLine[j]
        return(ibdict)        
    def main(self):
		# your code
        def Record_blat_length(blastfile):
            f = open(blastfile)
            txtLst = f.readlines()
            f.close()
            Num_col = len(txtLst[0].strip().split("\t"))
            if Num_col==12:
                VAR = ['Query','Subject','id','alignment length','mismatches','gap openings','q.start','q.end','s.start','s.end','e-value','bit score']
            elif Num_col==14:
                VAR = ['Query','Subject','id','alignment length','mismatches','gap openings','q.start','q.end','s.start','s.end','e-value','bit score','gaps','positive']
            else:
                return("Please enter a blat/blast result with 12 or 14 columns.")    
            bdict = dict([])
            ibdict = dict([])
            anct = 0
            for i in range(len(txtLst)):
                txtLine = txtLst[i].strip().split()
                qname = txtLine[0]
                # filters for valid alignment (id >= 90%, gap sum <= 5)
                if float(txtLine[2]) < 90:
                    continue
                if Num_col == 14 and (int(txtLine[5])+int(txtLine[12]))>5:
                    continue
                #format chrX:q.start-q.end    Hclipped, Mlen, Tclipped
                Hclipped = int(txtLine[6]) - 1
                Mlen = int(txtLine[7]) - int(txtLine[6]) + 1
                if qname not in bdict:
                        bdict[qname] = anct
                        ibdict[anct] = {}
                        for j in range(len(VAR)):
                            ibdict[anct][VAR[j]] = txtLine[j]
                        ibdict[anct]['count'] = 1                
                        ibdict[anct]['q.end.self'] = 0
                        ibdict[anct]['q.end.self'] = ibdict[anct]['q.end'] 
                        ibdict[anct]['loci'] = []
                        ibdict[anct]['AID'] = []
                        Tclipped = int(ibdict[anct]['q.end.self']) - int(txtLine[7]) #shall be 0 for self-alignment #debug
                        loci = txtLine[1] + ":" + txtLine[6] + "-" + txtLine[7] + "\t" + str(Hclipped) + "\t" + str(Mlen) + "\t" + str(Tclipped) #debug
                        ibdict[anct]['loci'].append([loci])
                        ibdict[anct]['AID'].append(i)
                        anct+=1
                else:
                    Tclipped = int(ibdict[bdict[qname]]['q.end.self']) - int(txtLine[7]) #debug
                    if Mlen < int(ibdict[bdict[qname]]['q.end.self']): # filter for valid alignment (# aligned ratio == 1)
                        continue
                    ibdict[bdict[qname]]['count'] += 1 #number of "valid" secondary alignments 
                    loci = txtLine[1] + ":" + txtLine[6] + "-" + txtLine[7] + "\t" + str(Hclipped) + "\t" + str(Mlen) + "\t" + str(Tclipped) #debug
                    ibdict[bdict[qname]]['loci'].append([loci])
                    ibdict[bdict[qname]]['AID'].append(i)
            # iterate ibdict and eliminate qname without "valid" secondary alignment
            for qname in bdict:
                if ibdict[bdict[qname]]['count'] == 1:
                    del ibdict[bdict[qname]]                   
            return(ibdict)
        try:
            Blatresult = Record_blat_length(self.vars['input_file']) #step 1
        except:
            print("your file does not exist!")
        # main code
        outname = args.output_dir + ".ps.key.info.csv"

        # a function to record all alignments
        def Record_all_alignments(blat_length_file):
            Merged_list = dict([])
            Merged_list['Query_ID'] = []
            Merged_list['Lengths'] = []
            Merged_list['AID'] = []
            for i in blat_length_file:
                for Locinum in range(len(blat_length_file[i]['loci'])):
                    Merged_list['Query_ID'].append(blat_length_file[i]['Query'])
                    Merged_list['Lengths'].append("".join(blat_length_file[i]['loci'][Locinum]))
                    Merged_list['AID'].append(blat_length_file[i]['AID'][Locinum])
            #Data frame for each alignment
            Dataframe = pd.DataFrame(Merged_list) 
            return(Dataframe)
        # a function to split column values
        def split_str(COL):#input a column you want to split
            Results = dict([])
            Newcolumns = dict([])
            #split data
            for i in range(len(COL)):
                Ele_num = len(COL[i].split()) #number of elements after splitting
                Results[i] = COL[i].split() # key: row index, value: a list of splitting elements
            #create columns
            for i in range(len(COL[0].split())):#4 columns
                Newcolumns[i] = []
            for i in range(len(COL)):
                for j in range(Ele_num): #add elements to the columns
                    Newcolumns[j].append(Results[i][j])
            return(Newcolumns)
        
        #step 3: store 5'- and 3'-end clipping length and aligned length 
        df = Record_all_alignments(Record_blat_length(self.vars['input_file']))
        Cols = split_str(df["Lengths"])
        df["Hclipped"]= Cols[1]
        df["Mlen"] = Cols[2]
        df["Tclipped"] =Cols[3]
        df = df.drop("Lengths",axis=1)

        #step 4: extract self alignment length
        #df["AID"] = df.index
        BLAT_self = df.copy()
        BLAT_self.drop_duplicates(subset=["Query_ID"],inplace=True)
        BLAT_self.rename({'Mlen':'Self'},axis=1,inplace=True)     
                
        #step 5: generate distance column
        New_result = df.merge(BLAT_self[['Query_ID','Self']])
        New_result["Distance"] = pd.to_numeric(New_result["Self"]) - pd.to_numeric(New_result["Mlen"])
        df = New_result
        
        #step 6: read all alignments and store them in a dataframe
        selectedIDlist = list(df["AID"].unique())
        outputdict = self.Record_blat_raw2(self.vars['input_file'],selectedIDlist)
        blastndf = pd.DataFrame(outputdict).T
        blastndf["AID"] = blastndf.index
        # merge 2 dataframes
        if blastndf.shape[1] == 13:
            df2 = df.merge(blastndf[['AID','alignment length','id','mismatches','gap openings','Subject','s.start','s.end']])
        elif blastndf.shape[1] == 15:
            df2 = df.merge(blastndf[['AID','alignment length','id','mismatches','gap openings','gaps','Subject','s.start','s.end']])

        #step 7: store the dataframe to blat.key.info.csv file
        df2.to_csv(r"".join(outname),index=False,header=True) #shall remove this line
        

if __name__ == '__main__':
# code comment is required
    import argparse
    parser = argparse.ArgumentParser(description='Python code for blat key info (Mlen, Tclipped, Hclipped, id, mismatches, gap openings, (gaps)) extraction (output filename:blat.key.info.csv).')
    #parser.add_argument("-parameter", "-- parameter ", help="description for this parameter ", required=True, type=str)
    parser.add_argument("-i", "--input_file", help="input blat file.", required=True, type=str)
    parser.add_argument("-o", "--output_dir", help="result directory.", required=True, type=str)
    args = parser.parse_args()
    a = pkg(vars(args))
    a.main()