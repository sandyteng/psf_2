#!/usr/bin/python3
#======
#
# Program: Extract IDs from two .pseudo.list files
# Usage: 
# enter two .pseudo.list files and this program will generate a file containing a subset of the two files (a .pseudo.subset file)
# Description: 
# enter "u" for union, "d" for (union - intersection), "i" for intersection  
# Author: sandyteng (sandyteng@actgenomics.com)
#
#======
import pandas as pd

class pkg():
    def __init__(self, args = {}):
        self.args = {}
        self.args.update(args)
        self.vars = {}
        self.prepare()

    def prepare(self):
        # pass information from args to vars
        self.vars['input_file'] = self.args['input_file'] # megablast result: /mnt/BI3/Team_workdir/sandyteng_workdir/docker/python_code/RESULTS/megablast.pseudo.list
        self.vars['input_file2'] = self.args['input_file2'] # blat result: /mnt/BI3/Team_workdir/sandyteng_workdir/docker/python_code/RESULTS/blat2.1.pseudo.list
        self.vars['output_file'] = self.args['output_file'] # output directory: /mnt/BI3/Team_workdir/sandyteng_workdir/python_code/RESULTS/blat_megablast
        self.vars['token'] = self.args['token'] # a token for creating subset (either u or d or i)

    def main(self):
		# your code
        try:
            sumfile = open(self.vars['input_file'])
            sumfile2 = open(self.vars['input_file2'])
        except:
            print("your file does not exist!")
        # main code
        outname = args.output_file + ".pseudo.subset"
        outfile = open(outname,'w')
        #extract IDs 
        def ReadLst(Path):
            IDlist = open(Path)
            IDLst = IDlist.readlines()
            IDs = list()
            for i in range(len(IDLst)):
                IDs.append(IDLst[i].strip())
            IDlist.close()
            return(IDs)
        set1 = set(ReadLst(self.vars['input_file']))
        set2 = set(ReadLst(self.vars['input_file2']))
        #create a subset of (A & B)
        if self.vars['token'] == "u":
            newlist = list(set1.union(set2)) #the union for set1 and set2
        elif self.vars['token'] == "d":
            newlist = list(set1.union(set2)-set1.intersection(set2)) #the set containing inconsistent read IDs
        elif self.vars['token'] == "i":
            newlist = list(set1.intersection(set2)) #the intersection for set1 and set2
        else:
            newlist = list()                
        for i in range(len(newlist)):
            print(newlist[i],file=outfile)
        # close files
        sumfile.close()
        sumfile2.close()
        outfile.close()

if __name__ == '__main__':
# code comment is required
    import argparse
    parser = argparse.ArgumentParser(description='Read info from your input file and create an output file.')
    parser.add_argument("-i", "--input_file", help="input file.", required=True, type=str)
    parser.add_argument("-ii", "--input_file2", help="input file2.", required=True, type=str)
    parser.add_argument("-o", "--output_file", help="result table.", required=True, type=str)
    parser.add_argument("-t", "--token", help="token to decide the subset to be created.", required=True, type=str)
    args = parser.parse_args()
    a = pkg(vars(args))
    a.main()