#!/usr/bin/python3
#======
#
# Program: extract all hits from infofiles  
# Usage: to extract all hits from unprocessed results (i.e., .info.csv file)
# Description: 
# Author: sandyteng (sandyteng@actgenomics.com)
#
#======
import pandas as pd

class pkg():
    def __init__(self, args = {}):
        self.args = {}
        self.args.update(args)
        self.vars = {}
        self.prepare()

    def prepare(self):
        # pass information from args to vars
        self.vars['input_file'] = self.args['input_file'] #e.g., /app/docker/python_code/RESULTS/blat.info.csv
        self.vars['output_file'] = self.args['output_file'] #e.g., /app/docker/python_code/RESULTS/blat
        
    def main(self):
		# your code
        try:
            sumfile = open(self.vars['input_file'])
        except:
            print("your file does not exist!")
        # main code
        outname = args.output_file + ".all"
        outfile = open(outname,'w')
        DATA = pd.read_csv(args.input_file)
        df2 = DATA.drop_duplicates(subset=['Query_ID'],keep='first')
        IDlist = list(df2['Query_ID'])
        for element in IDlist:
            print(element,file=outfile)

        # close files
        sumfile.close()
        outfile.close()

if __name__ == '__main__':
# code comment is required
    import argparse
    parser = argparse.ArgumentParser(description='Read info from your input file and create an output file.')
    #parser.add_argument("-parameter", "-- parameter ", help="description for this parameter ", required=True, type=str)
    parser.add_argument("-i", "--input_file", help="input file.", required=True, type=str)
    parser.add_argument("-o", "--output_file", help="result table.", required=True, type=str)
    args = parser.parse_args()
    a = pkg(vars(args))
    a.main()